Rails.application.routes.draw do
  get 'auth/failure', to: 'sessions#new'

  controller :sessions do
    get '/auth/:provider/callback', action: :create
    get  'login', action: :new
    post 'login', action: :create
    get 'logout', action: :destroy
  end

  resources :lists do
    resources :tasks do
      patch :complete, on: :member
    end
  end

  root to: 'lists#index'
end
