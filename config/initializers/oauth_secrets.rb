Rails.application.secrets.google_oauth_client_id = ENV["CLIENT_ID"]
Rails.application.secrets.google_oauth_client_secret = ENV["CLIENT_SECRET"]
