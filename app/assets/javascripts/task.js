function Task(elements){
  this.$tasksContainer = elements.$tasksContainer;
}

Task.prototype.init = function () {
  this.bindEvents();
};

Task.prototype.bindEvents = function () {
  var _this = this;
  this.$tasksContainer.on('click', '[data-class="mark-completed-btn"]', _this.sendCompletionAjax());
  this.$tasksContainer.on('click', '[data-class="trash-btn"]', _this.sendDeleteAjax());
};

Task.prototype.sendCompletionAjax = function () {
  var _this = this;
  return function(){
    var $this = $(this);
    $this.closest('[data-task="true"]').attr('data-item', 'current');
    _this.sendAjax(_this.prepareOptionsForAjax($this));
  };
};

Task.prototype.sendDeleteAjax = function () {
  var _this = this;
  return function(){
    var $this = $(this);
    if(confirm($this.attr('data-confirm-message'))) {
      $this.closest('[data-task="true"]').attr('data-item', 'current');
      _this.sendAjax(_this.prepareOptionsForAjax($this));
    }
  };
};

Task.prototype.prepareOptionsForAjax = function ($current_element) {
  options = {
    url: $current_element.data('url'),
    method: $current_element.data('method')
  };
  return options;
};

Task.prototype.sendAjax = function (options) {
  var _this = this;
  $.ajax({
    url: options.url,
    dataType: 'json',
    method: options.method
  }).success(function(data) {
      _this.handleAjaxSuccess(data, options);
    }).error(function(xhr) {
      _this.handleAjaxFailure(xhr, options);
    });
};

Task.prototype.handleAjaxSuccess = function (data, options) {
  if(options.method == 'delete'){
    this.deleteTask();
  } else {
    this.markCompleted();
  }
  $('p #notice').html(data.notice);
};

Task.prototype.handleAjaxFailure = function (xhr, options) {
  $('p #notice').html($.parseJSON(xhr.responseText).errors);
};

Task.prototype.markCompleted = function () {
  $('[data-item="current"]').find('.task-description').addClass('strike-through').removeAttr('data-item');
};

Task.prototype.deleteTask = function () {
  $('[data-item="current"]').remove();
};

$(function(){
  elements = {
    $tasksContainer: $('.tasks-container'),
  };
  task = new Task(elements);
  task.init();
});
