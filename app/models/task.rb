class Task < ActiveRecord::Base
  belongs_to :list

  validates :list, :description, presence: true
  validates :description, length: { maximum: 200 }

  def completed?
    completed_at.present?
  end
end
