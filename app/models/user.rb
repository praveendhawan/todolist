class User < ActiveRecord::Base
  has_many :lists, dependent: :destroy
  has_many :tasks, through: :lists

  before_validation :set_permalink

  validates :name, :email, :permalink, presence: true
  validates :permalink, uniqueness: { allow_blank: true, case_sensitive: false }

  private

    def set_permalink
      self.permalink = self.email.split('@').first
    end
end
