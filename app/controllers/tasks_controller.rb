class TasksController < ApplicationController
  before_action :load_list, only: [:create, :destroy, :complete]
  before_action :load_task, only: [:destroy, :complete]

  def create
    @task = @list.tasks.create(tasks_params)
    respond_to do |format|
      if @task.save
        format.html { redirect_to @list, notice: t(:resource_created, resource: 'Task') }
        format.js
      else
        format.html redirect_to @list, notice: t(:resource_not_created, resource: 'Task')
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @task.destroy
        format.json { render json: { notice: t(:resource_destroyed, resource: 'Task') }, status: 200 }
      else
        format.json { render json: @task.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  def complete
    respond_to do |format|
      @task.update_attribute(:completed_at, Time.current)
      format.json { render json: { notice: t(:task_completed) }, status: 200 }
    end
  end

  private
    def load_list
      @list = List.find_by(id: params[:list_id])
      render json: { errors: [t(:resource_not_found, resource: 'List')] }, status: 404 unless @list
    end

    def load_task
      @task = Task.find_by(id: params[:id])
      render json: { errors: [t(:resource_not_found, resource: 'Task')] }, status: 404 unless @task
    end

    def tasks_params
      params.require(:task).permit(:id, :description)
    end
end
