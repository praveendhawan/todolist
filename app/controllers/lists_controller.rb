class ListsController < ApplicationController

  before_action :load_list, except: [:new, :create, :index]
  before_action :load_user, only: :create

  def index
    @lists = List.all
  end

  def new
    @list = List.new
  end

  def create
    @list = @user.lists.create(lists_params)
    respond_to do |format|
      if @list.save
        format.html { redirect_to @list, notice: t(:resource_created, resource: 'List') }
        format.json { render :show, status: :created, location: @list }
      else
        format.html { render :new }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit; end

  def update
    respond_to do |format|
      if @list.update(list_params)
        format.html { redirect_to @list, notice: t(:resouce_updated, resource: 'List') }
        format.json { render :show, status: :ok, location: @list }
      else
        format.html { render :edit }
        format.json { render json: @list.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @list.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: t(:resource_destroyed, resource: 'List') }
      format.json { head :no_content }
    end
  end

  def show; end

  private
    def lists_params
      params.require(:list).permit(:id, :title, tasks_attributes: [:id, :description, :_destroy])
    end

    def load_list
      @list = List.find_by(id: params[:id])
    end

    def load_user
      @user = current_user
    end
end
