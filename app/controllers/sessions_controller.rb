class SessionsController < ApplicationController
  before_action :reset_session, only: [:create, :destroy]
  before_action :set_or_create_user, only: :create

  skip_before_action :authorize

  def new; end

  def create
    session[:user_id] = @user.id if @user.present?
    if session[:user_id]
      redirect_to root_url, notice: t(:login_successful)
    else
      redirect_to login_url, alert: t(:login_failure)
    end
  end

  def destroy
    redirect_to login_url, notice: t(:logout_successful)
  end

  private

    def set_or_create_user
      if (extract_email)
        @user = User.find_by(email: @email) || User.create(name: @auth['info']['name'],
          email: @email
        )
      end
    end

    def extract_email
      @auth = request.env["omniauth.auth"]
      @email = @auth["info"]["email"] if @auth['info']
    end

end
